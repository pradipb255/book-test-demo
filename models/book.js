const { sequelize } = require("../dbConnection");
const { DataTypes } = require("sequelize");

const User = sequelize.define(
  "Book",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataTypes.STRING,
    },
    author: {
      type: DataTypes.STRING,
    },
    published_year: {
      type: DataTypes.STRING,
    },
  },
  {}
);

User.sync({
  // force: true,
});

module.exports = User;
