const { Sequelize } = require("sequelize");

const DB_NAME = "book-store";
const DB_USER_NAME = "postgres";
const DB_PASSWORD = "example";

const sequelize = new Sequelize(DB_NAME, DB_USER_NAME, DB_PASSWORD, {
  database: DB_NAME,
  dialect: "postgres",
});

const testConnection = () => {
  sequelize.authenticate().then(function (errors) {
    console.log(errors);
  });
};

module.exports = {
  testConnection,
  sequelize,
};
