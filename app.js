const express = require("express");
const app = express();
const bodyParser = require("body-parser");

// support parsing of application/json type post data
app.use(bodyParser.json());

/** all router */
const bookRouter = require("./book/route");

app.use("/health", (req, res) => {
  res.send({ message: "healthy" });
});

app.use("/book", bookRouter);

module.exports = app;
