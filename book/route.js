const router = require("express").Router();
const { bookModel } = require("../models");
const { validateAddBook, validateGetBook } = require("./validator");
const { Op } = require("sequelize");

router.get("/", validateGetBook, async (req, res) => {
  let { title, author, published_year, sort = "id", order = "asc" } = req.query;

  let findBookQuery = {};

  Object.keys(req.query).forEach((key) => {
    switch (key) {
      case "title":
      case "author":
      case "published_year":
        findBookQuery[key] = {
          [Op.iLike]: `%${req.query[key]}%`,
        };
        break;
      default:
        break;
    }
  });

  /** get all books data */
  let booksDetail = await bookModel.findAll({
    where: findBookQuery,
    order: [[sort, order]],
  });

  res.status(200).send({
    message: "Book list received successfully",
    body: {
      data: booksDetail,
    },
  });
});

router.post("/", validateAddBook, async (req, res) => {
  let { title, author, published_year } = req.body;

  /** create book */
  let bookData = await bookModel.create({
    title,
    author,
    published_year,
  });
  res.status(200).send({
    message: "Book detail saved successfully",
    body: {
      data: bookData,
    },
  });
});

module.exports = router;
