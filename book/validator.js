let validateAddBook = (req, res, next) => {
  let { title, published_year, author } = req.body;
  let errors = [];

  if (!title) {
    errors.push("title is required");
  }

  if (!published_year) {
    errors.push("published_year is required");
  }

  if (!author) {
    errors.push("author is required");
  }

  if (errors.length) {
    res.status(422).send({
      status: 422,
      message: "Precondition failed",
      errors,
    });
  } else {
    next();
  }
};

let validateGetBook = (req, res, next) => {
  let { order } = req.query;
  let errors = [];

  if (order && !["asc", "desc"].includes(String(order).toLowerCase)) {
    errors.push("invalid order value");
  }

  if (errors.length) {
    res.status(422).send({
      status: 422,
      message: "Precondition failed",
      errors,
    });
  } else {
    next();
  }
};

module.exports = {
  validateAddBook,
  validateGetBook,
};
