const chai = require("chai");
const expect = chai.expect;
const app = require("../app");
const request = require("supertest")(app);

describe("User route", () => {
  describe("GET /book", () => {
    it("should return a list of array", async () => {
      const response = await request.get("/book");
      expect(response.status).to.equal(200);
      expect(response.body).to.be.an("object");
    });
  });

  describe("GET /book", () => {
    it("should return a list of array", async () => {
      const response = await request.get("/book").query({ order: "descs" });

      expect(response.status).to.equal(422);
      expect(response.body).to.be.an("object");
      expect(response.body.errors).to.be.an("array");
      expect(response.body.message).to.equal("Precondition failed");
    });
  });

  describe("POST /book", () => {
    it("should return a 200 status code and a success message", async () => {
      const response = await request.post("/book").send({
        title: "Book Title 2",
        author: "Author 2",
        published_year: "2012",
      });
      expect(response.status).to.equal(200);
      expect(response.body).to.be.an("object");
      expect(response.body.message).to.equal("Book detail saved successfully");
    });
  });

  describe("POST /book", () => {
    it("should return a 422 status code", async () => {
      const response = await request.post("/book").send({
        title: "Book Title 2",
      });
      expect(response.status).to.equal(422);
      expect(response.body).to.be.an("object");
      expect(response.body.message).to.equal("Precondition failed");
    });
  });
});
